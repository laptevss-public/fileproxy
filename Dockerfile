FROM nginx:1.19.1

COPY default.conf /etc/nginx/conf.d/default.conf

RUN cd /tmp && \
    apt-get update && \
    apt-get download -y --quiet git && \
    mkdir -p /usr/share/nginx/html/git && \
    mv git* /usr/share/nginx/html/git/ && \
    apt-get -y --quiet --no-install-recommends install \
      tree \
      wget && \
    apt-get clean && \
    cd /usr/share/nginx/html/ && \
    cat /etc/nginx/conf.d/default.conf && \
    rm /usr/share/nginx/html/index.html && \
    mkdir -p \
      ldap \
      helm \
      lens \
      keepass \
      k9s/v0.25.18 \
      k9s/v0.24.15 \
      k9s/v0.23.10 \
      k9s/v0.22.1 \
      k9s/v0.21.10 \
      postman \
      git \
      rocketchat && \
    wget -P ./ldap https://dlcdn.apache.org/directory/studio/2.0.0.v20210717-M17/ApacheDirectoryStudio-2.0.0.v20210717-M17-win32.win32.x86_64.zip && \  
    wget -P ./k9s/v0.25.18 https://github.com/derailed/k9s/releases/download/v0.25.18/k9s_Linux_x86_64.tar.gz && \ 
    wget -P ./k9s/v0.25.18 https://github.com/derailed/k9s/releases/download/v0.25.18/k9s_Windows_x86_64.tar.gz && \
    wget -P ./k9s/v0.24.15 https://github.com/derailed/k9s/releases/download/v0.24.15/k9s_Linux_x86_64.tar.gz && \ 
    wget -P ./k9s/v0.24.15 https://github.com/derailed/k9s/releases/download/v0.24.15/k9s_Windows_x86_64.tar.gz && \
    wget -P ./k9s/v0.23.10 https://github.com/derailed/k9s/releases/download/v0.23.10/k9s_Linux_x86_64.tar.gz && \ 
    wget -P ./k9s/v0.23.10 https://github.com/derailed/k9s/releases/download/v0.23.10/k9s_Windows_x86_64.tar.gz && \
    wget -P ./k9s/v0.22.1  https://github.com/derailed/k9s/releases/download/v0.22.1/k9s_Linux_x86_64.tar.gz && \ 
    wget -P ./k9s/v0.22.1  https://github.com/derailed/k9s/releases/download/v0.22.1/k9s_Windows_x86_64.tar.gz && \
    wget -P ./k9s/v0.21.10 https://github.com/derailed/k9s/releases/download/v0.21.10/k9s_Linux_x86_64.tar.gz && \ 
    wget -P ./k9s/v0.21.10 https://github.com/derailed/k9s/releases/download/v0.21.10/k9s_Windows_x86_64.tar.gz && \
    wget -P ./lens https://api.k8slens.dev/binaries/Lens%20Setup%205.4.4-latest.20220325.1.exe && \
    wget -P ./keepass https://github.com/keepassxreboot/keepassxc/releases/download/2.7.0/KeePassXC-2.7.0-Win64.msi && \
    wget -P ./keepass https://github.com/keepassxreboot/keepassxc/releases/download/2.7.0/KeePassXC-2.7.0-Win64.zip && \
    wget -P ./helm https://get.helm.sh/helm-v3.1.2-linux-amd64.tar.gz && \
    wget -P ./helm https://get.helm.sh/helm-v3.1.2-windows-amd64.zip && \
    wget -P ./helm https://get.helm.sh/helm-v3.1.3-linux-amd64.tar.gz && \
    wget -P ./helm https://get.helm.sh/helm-v3.1.3-windows-amd64.zip && \
    wget -P ./helm https://get.helm.sh/helm-v3.6.2-linux-amd64.tar.gz && \
    wget -P ./helm https://get.helm.sh/helm-v3.6.2-windows-amd64.zip && \
    wget -P ./helm https://get.helm.sh/helm-v3.8.1-linux-amd64.tar.gz && \
    wget -P ./helm https://get.helm.sh/helm-v3.8.1-windows-amd64.zip && \
    wget -P ./git https://github.com/git-for-windows/git/releases/download/v2.35.1.windows.2/Git-2.35.1.2-64-bit.exe && \
    wget -P ./git https://github.com/git-for-windows/git/releases/download/v2.35.1.windows.2/PortableGit-2.35.1.2-64-bit.7z.exe && \
    wget -P ./git https://repo.ius.io/7/x86_64/packages/g/git224-2.24.4-1.el7.ius.x86_64.rpm && \
    wget -P ./rocketchat https://github.com/RocketChat/Rocket.Chat.Electron/releases/download/3.5.7/rocketchat-setup-3.5.7.exe && \
    wget -P ./postman https://dl.pstmn.io/download/latest/win64 && \
    mv ./postman/win64 ./postman/postman.exe && \
    tree /usr/share/nginx/html
